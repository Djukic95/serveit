export default {
    "Kuća i kancelarija": "https://image.flaticon.com/icons/svg/609/609803.svg",
    "Biznis": "https://image.flaticon.com/icons/svg/1055/1055644.svg",
    "Edukacija": "https://image.flaticon.com/icons/svg/167/167756.svg",
    "Vozila": "https://image.flaticon.com/icons/svg/265/265722.svg",
    "Ljepota i zdravlje": "https://image.flaticon.com/icons/svg/122/122454.svg",
    "Ostalo": "https://image.flaticon.com/icons/svg/164/164426.svg"
};