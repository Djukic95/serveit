export default {
    "Kuća i kancelarija": "https://images.freeimages.com/images/large-previews/4c2/at-home-1231031.jpg",
    "Biznis": "https://images.pexels.com/photos/1432942/pexels-photo-1432942.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    "Edukacija": "https://images.pexels.com/photos/374820/pexels-photo-374820.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    "Vozila": "https://images.pexels.com/photos/97075/pexels-photo-97075.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    "Ljepota i zdravlje": "https://images.pexels.com/photos/1282308/pexels-photo-1282308.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    "Ostalo": "https://images.pexels.com/photos/845451/pexels-photo-845451.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
};