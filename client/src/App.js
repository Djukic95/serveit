import React, {
  Component
} from 'react';
import {
  BrowserRouter,
  Redirect,
  Route,
  Switch
} from 'react-router-dom';
import {
  Container
} from 'reactstrap';

import AppNavbar from './components/AppNavbar';
import ServiceListForm from './components/ServiceListForm';
import LoginForm from './components/LoginForm';
import AddServiceForm from './components/AddServiceForm';
import NotFoundForm from './components/NotFoundForm';
import ServiceViewForm from './components/ServiceViewForm';
import UserViewForm from './components/UserViewForm';
import AppHome from './components/AppHome';
import AppFooter from './components/AppFooter';

import AdminLoginForm from './components/admin/AdminLoginForm';
import AdminHomeForm from './components/admin/AdminHomeForm';
import AdminStatisticsForm from './components/admin/AdminStatisticsForm';
import AdminServicesForm from './components/admin/AdminServicesForm';
import AdminUsersForm from './components/admin/AdminUsersForm';
import AccInfoEdit from './components/AccInfoEdit';
import PasswordEdit from './components/PasswordEdit';
import ServiceInfoEdit from './components/ServiceInfoEdit';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    sessionStorage.getItem('Bearer') && sessionStorage.getItem('username')
      ? <Component {...props} />
      : <LoginForm />
  )} />
);

const LoginRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    sessionStorage.getItem('Bearer') && sessionStorage.getItem('username')
      ? <Redirect to='/' />
      : <Component {...props} />
  )} />
);

const AdminLoginRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    sessionStorage.getItem('AdminBearer') && sessionStorage.getItem('adminUsername')
      ? <Redirect to='/admin' />
      : <Component {...props} />
  )} />
);

const AdminHomeRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    sessionStorage.getItem('AdminBearer') && sessionStorage.getItem('adminUsername')
      ? <Component {...props} />
      : <Redirect to='/admin/login' />
  )} />
);

const AdminStatsRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    sessionStorage.getItem('AdminBearer') && sessionStorage.getItem('adminUsername')
      ? <Component {...props} />
      : <Redirect to='/admin/login' />
  )} />
);

const AdminUsersRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    sessionStorage.getItem('AdminBearer') && sessionStorage.getItem('adminUsername')
      ? <Component {...props} />
      : <Redirect to='/admin/login' />
  )} />
);

const AdminServicesRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    sessionStorage.getItem('AdminBearer') && sessionStorage.getItem('adminUsername')
      ? <Component {...props} />
      : <Redirect to='/admin/login' />
  )} />
);


export default class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route path='/admin'>
              <Switch>
                <AdminHomeRoute exact path='/admin' component={AdminHomeForm} />
                <AdminLoginRoute exact path='/admin/login' component={AdminLoginForm} />
                <AdminStatsRoute exact path='/admin/statistics' component={AdminStatisticsForm} />
                <AdminUsersRoute exact path='/admin/users' component={AdminUsersForm} />
                <AdminServicesRoute exact path='/admin/services' component={AdminServicesForm} />
              </Switch>
            </Route>
            <Route>
              <Container>
                <AppNavbar />
                <Switch>
                  <Route exact path='/' component={AppHome} />
                  <Route exact path='/search' component={ServiceListForm} />
                  <PrivateRoute exact path='/edit/info' component={AccInfoEdit} />
                  <PrivateRoute exact path='/edit/passwd' component={PasswordEdit} />
                  <PrivateRoute path='/edit/service' component={ServiceInfoEdit}/>
                  <Route path='/services' component={ServiceViewForm} />
                  <Route path='/users' component={UserViewForm} />
                  <PrivateRoute exact path='/add' component={AddServiceForm} />
                  <LoginRoute exact path='/login' component={LoginForm} />
                  <Route component={NotFoundForm} />
                </Switch>
                <AppFooter />
              </Container>
            </Route>
          </Switch>
        </BrowserRouter>
      </div>);
  }
}
