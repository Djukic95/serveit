import React, {
    Component
} from 'react';

import {
    Container,
    Form,
    FormGroup,
    Label,
    Input,
    Alert,
    Button,
    Spinner
} from 'reactstrap';

import axios from 'axios';

export default class ServiceInfoEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            owner: null,
            id: null,
            title: 'loading...',
            description: 'loading...',
            clicked: false,
            success: false
        }

        this.onChange = this.onChange.bind(this);
    }

    componentWillMount() {
        var id = sessionStorage.getItem('serviceEditId');

        //checking if the session object id is the same as the one in the url
        if (window.location.href.split('/').slice(-1)[0] !== id) window.location.href = '/';

        //if the session object exists
        if (id !== undefined) {
            axios.get('/api/services/' + id)
                .then(res => {
                    this.setState({
                        id: res.data._id,
                        title: res.data.title,
                        description: res.data.description,
                        owner: res.data.owner
                    }, () => {
                        if (sessionStorage.getItem('username') !== this.state.owner) window.location.href = '/';
                    });
                }).catch(_err => {
                    window.location.href = '/';
                })
        }
    }

    onChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.setState({
            clicked: true
        });

        axios.put('/api/services/' + this.state.id, {
            description: this.state.description
        }, {
                headers: {
                    authorization: `Bearer ${sessionStorage.getItem('Bearer')}`
                }
            })
            .then(res => {
                if (res.status === 200) {
                    this.setState({
                        success: true
                    });
                    setTimeout(() => {
                        window.location.href = `/users/${sessionStorage.getItem('username')}`;
                    }, 1000);
                }
            }).catch(_err => {
                this.setState({
                    success: false,
                    clicked: false
                });
            });
    }

    render() {
        return (
            <Container className="container-info">
                <Form onSubmit={this.onSubmit}>
                    <FormGroup>
                        <Label size='sm' for="title">Naziv</Label>
                        <legend id="title" >{this.state.title}</legend>
                    </FormGroup>
                    <FormGroup>
                        <Label size='sm' for="description">Opis</Label>
                        <Input bsSize='sm' type="textarea" id="description" rows={5} value={this.state.description} onChange={this.onChange}></Input>
                    </FormGroup>
                    <FormGroup>
                        <Alert color='success' hidden={!this.state.success}>
                            Izmjene su uspješno sačuvane
                        </Alert>
                    </FormGroup>
                    <FormGroup>
                        {
                            this.state.clicked ?
                                <div className='d-flex justify-content-center'>
                                    <Spinner color='warning' />
                                </div> :
                                <Button outline size='sm' color='warning' block>SAČUVAJ IZMJENE</Button>
                        }
                    </FormGroup>
                </Form>
            </Container>
        )
    }
}