import React, {
    Component
} from 'react';
import {
    Alert,
    Button,
    Collapse,
    Container,
    Form,
    FormFeedback,
    FormGroup,
    Input,
    Label,
    Spinner
} from 'reactstrap';
import axios from 'axios';
import cities from '../static_data/cities';

export default class AccInfoEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            delete: false,
            notSure: false,
            gone: false,
            username: 'loading...',
            name: 'loading...',
            surname: 'loading...',
            contact: {
                email: 'loading...',
                phone: 'loading...',
                website: 'loading...'
            },
            street: '',
            city: 'loading...',
            businessHours: {
                weekday: 'loading...',
                saturday: 'loading...',
                sunday: 'loading...'
            }
        };
        this.iamSure = '';

        axios.get(`/api/users/${sessionStorage.getItem('username')}`)
            .then(res => {
                this.setState({
                    ...res.data,
                    color: res.data.isEnterprise ? 'premium-user' : 'ordinary-user'
                });
            }).catch(() => {
                window.location.href = '/login';
            });
    }

    onChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    onBusinessHoursChange = (e) => {
        this.setState({
            businessHours: {
                ...this.state.businessHours,
                [e.target.id]: e.target.value
            }
        });
    }

    onContactChange = (e) => {
        this.setState({
            contact: {
                ...this.state.contact,
                [e.target.id]: e.target.value
            }
        });
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.setState({
            clicked: true
        });

        //validation
        if (this.state.name.trim().length === 0 ||
            this.state.surname.trim().length === 0 ||
            this.state.city.trim().length === 0) {
            this.setState({
                notValid: true,
                clicked: false
            })
        } else {
            this.setState({
                notValid: false
            })

            //if the execution is here then it means the input is valid

            axios.put('/api/users/me', {
                name: this.state.name,
                surname: this.state.surname,
                street: this.state.street,
                city: this.state.city,
                contact: {
                    email: this.state.contact.email,
                    phone: this.state.contact.phone,
                    website: this.state.contact.website
                },
                businessHours: {
                    weekday: this.state.businessHours.weekday,
                    saturday: this.state.businessHours.saturday,
                    sunday: this.state.businessHours.sunday
                }
            }, {
                    headers: {
                        authorization: `Bearer ${sessionStorage.getItem('Bearer')}`
                    }
                })
                .then(res => {
                    if (res.status === 200) {
                        this.setState({
                            success: true
                        });
                        setTimeout(() => {
                            window.location.href = `/users/${sessionStorage.getItem('username')}`;
                        }, 1000);
                    }
                }).catch(_err => {
                    this.setState({
                        success: false,
                        clicked: false
                    });
                });
        }

    }

    onDelete = (e) => {
        e.preventDefault();
        this.iamSure = this.iamSure.trim().toLowerCase();
        if (this.iamSure === 'siguran sam' || this.iamSure === 'sigurna sam') {
            this.setState({
                notSure: false
            });
            axios.delete('/api/users/me', {
                headers:
                    { authorization: `Bearer ${sessionStorage.getItem('Bearer')}` }
            })
                .then(() => {
                    this.setState({
                        gone: true
                    });
                    sessionStorage.clear();
                    document.getElementById('alert-text').innerHTML = 'Jako nam je žao što nas napuštate. ' +
                        'Bićete odjavljeni za nekoliko trenutaka, a sve vaše usluge će nestati iz sistema.'
                    setTimeout(() => {
                        window.location.pathname = '/';
                    }, 5000);
                });
        } else {
            this.setState({
                notSure: true
            });
        }
    }

    render() {
        return (
            <Container className='container-form'>
                <Collapse isOpen={!this.state.delete}>
                    <Form onSubmit={this.onSubmit}>
                        <h4>OSNOVNE INFORMACIJE</h4>
                        <FormGroup>
                            <Label size='sm' for='name'>Ime <span style={{ "color": "red" }}>*</span></Label>
                            <Input bsSize='sm' id='name' value={this.state.name} disabled={this.state.clicked} onChange={this.onChange} />
                        </FormGroup>
                        <FormGroup>
                            <Label size='sm' for='surname'>Prezime <span style={{ "color": "red" }}>*</span></Label>
                            <Input bsSize='sm' id='surname' value={this.state.surname} disabled={this.state.clicked} onChange={this.onChange} />
                        </FormGroup>
                        <FormGroup>
                            <Label size='sm' for='street'>Adresa</Label>
                            <Input bsSize='sm' id='street' value={this.state.street} disabled={this.state.clicked} onChange={this.onChange} />
                        </FormGroup>
                        <FormGroup>
                            <Label size='sm' for='city'>Grad <span style={{ "color": "red" }}>*</span></Label>
                            <Input bsSize='sm' value={this.state.city} type='select' id='city' disabled={this.state.clicked} onChange={this.onChange} >
                                {cities.map((city) => (<option key={city}>{city}</option>))}
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label size='sm' for='email'>E-mail</Label>
                            <Input bsSize='sm' type='email' id='email' value={this.state.contact.email} disabled={this.state.clicked} onChange={this.onContactChange} />
                        </FormGroup>
                        <FormGroup>
                            <Label size='sm' for='phone'>Telefon </Label>
                            <Input bsSize='sm' id='phone' value={this.state.contact.phone} disabled={this.state.clicked} onChange={this.onContactChange} />
                        </FormGroup>
                        <FormGroup>
                            <Label size='sm' for='website'>Websajt</Label>
                            <Input bsSize='sm' id='website' value={this.state.contact.website} disabled={this.state.clicked} onChange={this.onContactChange} />
                        </FormGroup>
                        <h4 className='mt-5'>RADNO VRIJEME</h4>
                        <FormGroup>
                            <Label size='sm' for='weekday'>Radni dan</Label>
                            <Input bsSize='sm' id='weekday' placeholder='08:00 - 16:00' value={
                                this.state.businessHours.weekday !== '' ?
                                    this.state.businessHours.weekday : ''
                            } disabled={this.state.clicked} onChange={this.onBusinessHoursChange} />
                        </FormGroup>
                        <FormGroup>
                            <Label size='sm' for='saturday'>Subota</Label>
                            <Input bsSize='sm' id='saturday' placeholder='09:00 - 15:00' value={
                                this.state.businessHours.saturday !== '' ?
                                    this.state.businessHours.saturday : ''
                            } disabled={this.state.clicked} onChange={this.onBusinessHoursChange} />
                        </FormGroup>
                        <FormGroup>
                            <Label size='sm' for='sunday'>Nedjelja</Label>
                            <Input bsSize='sm' id='sunday' placeholder='09:00 - 13:00' value={
                                this.state.businessHours.sunday !== '' ?
                                    this.state.businessHours.sunday : ''
                            } disabled={this.state.clicked} onChange={this.onBusinessHoursChange} />
                        </FormGroup>
                        <FormGroup>
                            <Alert color='success' hidden={!this.state.success}>
                                Izmjene su uspješno sačuvane
                        </Alert>
                            <Alert color='danger' hidden={!this.state.notValid}>
                                Molimo Vas da popunite sva obavezna polja. Obavezna polja su označena sa *
                        </Alert>
                        </FormGroup>
                        <FormGroup>
                            {
                                this.state.clicked ?
                                    <div className='d-flex justify-content-center'>
                                        <Spinner color='warning' />
                                    </div> :
                                    <Button size='sm' outline color='warning' block>SAČUVAJ IZMJENE</Button>
                            }
                        </FormGroup>
                    </Form>
                    <h5 className='hand mt-5' style={{ color: 'red' }} onClick={() => {
                        this.setState({
                            delete: !this.state.delete
                        });
                    }}> --- &#10006; ŽELIM UKLONITI NALOG ---</h5>
                </Collapse>

                <Collapse isOpen={this.state.delete}>
                    <h5 className='hand mt-5' style={{ color: 'orange' }} onClick={() => {
                        this.setState({
                            delete: !this.state.delete
                        });
                    }}> --- &#10000; ŽELIM IZMIJENITI NALOG ---</h5>
                    <Form onSubmit={this.onDelete}>
                        <FormGroup>
                            <Alert color='dark' id='alert-text'>
                                Uklanjanje naloga je nepovratna akcija, a
                                uklanjanjem naloga se takođe uklanjaju i sve
                                Vaše objavljene usluge. Ako ste sigurni da
                                želite ukloniti nalog u polje ispod unesite
                                text <b>siguran/na sam</b>.
                            </Alert>
                            <Input bsSize='sm' hidden={this.state.gone} invalid={this.state.notSure} onChange={(e) => { this.iamSure = e.target.value }}></Input>
                            <FormFeedback>
                                Vaš nalog neće biti uklonjen sve dok ne potvrdite da ste sigurni
                            </FormFeedback>
                        </FormGroup>
                        <Button outline block size='sm' color='danger' hidden={this.state.gone}>UKLONI MOJ NALOG</Button>
                    </Form>
                </Collapse>
            </Container>
        );
    }
}