import React, {
    Component
} from 'react';
import {
    Modal,
    ModalHeader,
    ModalBody,
    NavLink,
    NavItem
} from 'reactstrap';
import LoginForm from './LoginForm';

export default class LoginModal extends Component {
    state = {
        isOpen: false
    }

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
            <div>
                <NavItem className='hand'>
                    <NavLink id='login-btn' onClick={this.toggle}>PRIJAVI SE</NavLink>
                </NavItem>
                <Modal isOpen={this.state.isOpen} toggle={this.toggle} autoFocus={false}>
                    <ModalHeader toggle={this.toggle}>PRIJAVI SE</ModalHeader>
                    <ModalBody>
                        <LoginForm />
                    </ModalBody>
                </Modal>
            </div>
        );
    }
}