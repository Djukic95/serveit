import React, {
    Component
} from 'react';
import {
    Jumbotron,
    Container,
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    CarouselCaption
} from 'reactstrap';
import axios from 'axios';
import SearchBar from './SearchBar';
import images from '../static_data/images';

export default class AppHome extends Component {
    state = {
        activeIndex: 0,
        loaded: false,
        services: [],
        items: [],
        usercount: 0,
        servicecount: 0
    }

    constructor(props) {
        super(props);
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.goToIndex = this.goToIndex.bind(this);
        this.onExiting = this.onExiting.bind(this);
        this.onExited = this.onExited.bind(this);
    }

    onExiting() {
        this.animating = true;
    }

    onExited() {
        this.animating = false;
    }

    next() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === this.state.items.length - 1 ? 0 : this.state.activeIndex + 1;
        this.setState({ activeIndex: nextIndex });
    }

    previous() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === 0 ? this.state.items.length - 1 : this.state.activeIndex - 1;
        this.setState({ activeIndex: nextIndex });
    }

    goToIndex(newIndex) {
        if (this.animating) return;
        this.setState({ activeIndex: newIndex });
    }

    jumbotronStyle = {
        "backgroundImage": "url(https://www.languago.com/wp-content/uploads/2016/09/Handshake-1.jpg)",
        "backgroundSize": "cover"
    };

    componentWillMount() {
        axios.get(`/api/services/last/5`)
            .then(res => {
                this.setState({
                    loaded: true,
                    services: res.data
                });
                //creating the item list
                for (var i = 0; i < this.state.services.length; i++) {
                    this.state.items.push({
                        altText: this.state.services[i].title,
                        captionHeader: this.state.services[i].category,
                        captionText: this.state.services[i].description,
                        itemPath: 'services/' + this.state.services[i]._id
                    });
                }
            }).catch(() => console.log('App is down'));

        axios.get('/api/users/count').then(res => {
            this.setState({
                usercount: res.data
            })
        }).catch(() => console.log('counter is down'));

        axios.get('/api/services/count').then(res => {
            this.setState({
                servicecount: res.data
            })
        }).catch(() => console.log('counter is down'));
    }


    //has to have search bar, jumbotron for presentation and carousel for 5 last services
    render() {
        const { activeIndex } = this.state;

        const slides = this.state.items.map((item) => (
            <CarouselItem
                onExiting={this.onExiting}
                onExited={this.onExited}
                key={item.itemPath}
            >
                <a href={item.itemPath}><img style={{ "width": "100%", "height": "550px" }} src={images[item.captionHeader]} alt={item.altText} /></a>
                <CarouselCaption className="slider-text" captionText={item.captionHeader} captionHeader={item.altText} />
            </CarouselItem>
        )
        );

        return (
            <Container>
                <SearchBar />
                <Jumbotron style={this.jumbotronStyle}>
                    <h1>Dobro došli na Serve <span style={{ "color": "orange" }}>IT</span>!</h1>
                    <br /><br />
                    <div style={{"width":"70%"}}>
                        <p>Serve IT je vodeća svjetska platforma za dijeljenje i pronalaženje usluga koje vam trebaju, na lak i brz način.
                        Pronađite šta vam treba u samo nekolko klikova i podijelite vaše usluge besplatno!
                        </p>
                    </div>
                    <hr />
                    <div className="home-cards">
                        <div className="home-card">
                            <p className="home-count">6</p>
                            <p>KATEGORIJA</p>
                        </div>
                        <div className="home-card">
                            <p className="home-count">{this.state.usercount}</p>
                            <p>KORISNIKA</p>
                        </div>
                        <div className="home-card">
                            <p className="home-count">{this.state.servicecount}</p>
                            <p>USLUGA</p>
                        </div>
                    </div>
                </Jumbotron>
                <div className="home-middle">
                    NAJNOVIJE <span style={{ "color": "orange" }}>USLUGE</span>
                </div>
                <Carousel
                    activeIndex={activeIndex}
                    next={this.next}
                    previous={this.previous}>
                    <CarouselIndicators items={this.state.items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
                    {slides}
                    <CarouselControl direction="prev" directionText="Prethodna" onClickHandler={this.previous} />
                    <CarouselControl direction="next" directionText="Iduća" onClickHandler={this.next} />
                </Carousel>
                <br />
                <br />
            </Container>
        );
    }
}