import React, {
    Component
} from 'react';
import {
    Alert,
    Button,
    Container,
    Form,
    FormFeedback,
    FormGroup,
    Input,
    Label,
    Spinner
} from 'reactstrap';
import axios from 'axios';

export default class AccInfoEdit extends Component {

    state = {
        clicked: false,
        invalidOld: false,
        invalidNew: false
    }

    passwd = {
        old: '',
        new1: '',
        new2: ''
    }

    onChange = (e) => {
        this.passwd[e.target.id] = e.target.value;
    }

    onSubmit = (e) => {
        e.preventDefault();
        if (this.passwd.old === '') {
            this.setState({
                invalidOld: true,
                invalidNew: false
            });
            return;
        }
        if (this.passwd.new1 !== this.passwd.new2 || this.passwd.new1 === '') {
            this.setState({
                invalidNew: true,
                invalidOld: false
            });
            return;
        }
        this.setState({
            clicked: true,
            success: false,
            invalidNew: false,
            invalidOld: false
        });

        axios.post('/api/users/passwd', {
            oldP: this.passwd.old, newP: this.passwd.new1
        }, {
                headers: {
                    authorization: `Bearer ${sessionStorage.getItem('Bearer')}`
                }
            })
            .then(res => {
                if (res.status === 200) {
                    this.setState({
                        success: true
                    });
                    setTimeout(() => {
                        window.location.href = `/users/${sessionStorage.getItem('username')}`;
                    }, 1000);
                }
            }).catch(err => {
                if (err.response) {
                    if (err.response.status === 409)
                        this.setState({
                            invalidOld: true,
                            clicked: false
                        });
                } else {
                    console.log('App is down');
                }
            });
    }

    render() {
        return (
            <Container className='container-form'>
                <Form onSubmit={this.onSubmit}>
                    <FormGroup>
                        <Label size='sm' for='old'>Unesite trenutnu lozinku</Label>
                        <Input bsSize='sm' invalid={this.state.invalidOld} id='old' type='password' disabled={this.state.clicked} onChange={this.onChange} />
                        <FormFeedback >Pogrešna lozinka</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                        <Label size='sm' for='new1'>Unesite novu lozinku</Label>
                        <Input bsSize='sm' invalid={this.state.invalidNew} id='new1' type='password' disabled={this.state.clicked} onChange={this.onChange} />
                        <FormFeedback >Lozinke se ne podudaraju</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                        <Label size='sm' for='new2'>Ponovo unesite novu lozinku</Label>
                        <Input bsSize='sm' invalid={this.state.invalidNew} id='new2' type='password' value={this.state.street} disabled={this.state.clicked} onChange={this.onChange} />
                        <FormFeedback>Lozinke se ne podudaraju</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                        <Alert color='success' hidden={!this.state.success}>
                            Lozinka je promjenjena
                     </Alert>
                    </FormGroup>
                    <FormGroup>
                        {
                            this.state.clicked ?
                                <div className='d-flex justify-content-center'>
                                    <Spinner color='warning' />
                                </div> :
                                <Button outline block size='sm' color='warning'>POTVRDI</Button>
                        }
                    </FormGroup>
                </Form>
            </Container>
        );
    }
}