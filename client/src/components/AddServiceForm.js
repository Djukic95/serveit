import React, {
    Component
} from 'react';
import {
    Alert,
    Button,
    Container,
    Form,
    FormGroup,
    Input,
    Label,
    Spinner
} from 'reactstrap';
import axios from 'axios';
import categories from '../static_data/categories';

export default class AddServiceForm extends Component {

    state = {
        category: null,
        clicked: false
    }

    service = {
        category: categories[0]
    }

    onChange = (e) => {
        this.service[e.target.id] = e.target.value
    }

    onSubmit = (e) => {
        e.preventDefault();

        if (!this.service.category || !this.service.title) {
            this.setState({
                alert: true,
            });
            return;
        }

        this.setState({
            alert: false,
            clicked: true
        });

        axios.post('/api/services/new', this.service, {
            headers: {
                authorization: 'Bearer ' + sessionStorage.getItem('Bearer')
            }
        }).then(res => {
            if (res.status === 200) {
                this.setState({
                    success: true
                });
                setTimeout(() => {
                    window.location.href = `/users/${sessionStorage.getItem('username')}`;
                }, 1000);
            }
        }).catch(_err => {
            this.setState({
                alert: true,
                clicked: false
            });
        });
    }

    render() {
        return (
            <Container className='container-form'>
                <h4>DODAJ USLUGU</h4>
                <br />
                <Form onSubmit={this.onSubmit}>
                    <FormGroup>
                        <Label size='sm' for="title">NASLOV <span style={{ "color": "red" }}>*</span></Label>
                        <Input bsSize='sm' id="title" disabled={this.state.clicked} onChange={this.onChange} />
                    </FormGroup>
                    <FormGroup>
                        <Label size='sm' for='category'>KATEGORIJA <span style={{ "color": "red" }}>*</span></Label>
                        <Input bsSize='sm' type='select' id='category' disabled={this.state.clicked} onChange={this.onChange} >
                            {categories.map((cat) => (<option key={cat}>{cat}</option>))}
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <Label size='sm' for='description'>OPIS</Label>
                        <Input bsSize='sm' type='textarea' rows={10} disabled={this.state.clicked} id='description' onChange={this.onChange} />
                    </FormGroup>
                    <FormGroup>
                        <Alert color='danger' hidden={!this.state.alert}>
                            Molimo Vas da popunite sva obavezna polja. Obavezna polja markirana su sa *
                        </Alert>
                        {
                            this.state.clicked ?
                                <div className='d-flex justify-content-center'>
                                    <Spinner color='warning' />
                                </div> :
                                <Button outline color='warning' size='sm' block>PRIHVATI</Button>
                        }
                    </FormGroup>
                </Form>
            </Container>
        );
    }
}