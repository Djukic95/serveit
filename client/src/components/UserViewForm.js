import React, {
    Component
} from 'react';
import {
    Container
} from 'reactstrap';
import UserInfo from './UserInfo';
import UsersServices from './UsersServices';
import SearchBar from './SearchBar';


export default class UserViewForm extends Component {

    componentWillMount() {
        this.username = window.location.pathname.substr(7);
        if (this.username.length < 1) {
            window.location.pathname = '/notfound';
        }
    }

    render() {
        return (
            <div>
                <Container>
                    <SearchBar />
                    <br /><br />
                </Container>
                <UserInfo username={this.username} />
                <div style={{ "width": "80%", "margin": "0 auto", "textAlign": "center" }}>
                    <h3>USLUGE</h3>
                    <hr></hr>
                </div>
                <UsersServices username={this.username} />
            </div>
        );
    }
}