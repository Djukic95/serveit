import React, {
    Component
} from 'react';
import {
    Col,
    Container,
    Input,
    InputGroup,
    InputGroupAddon,
    Row,
    Tooltip
} from 'reactstrap';
import axios from 'axios';

export default class UserInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editTooltipOpen: false,
            editPasswordTooltipOpen: false,
            username: 'loading...',
            name: 'loading...',
            surname: 'loading...',
            contact: {
                phone: '',
                website: '',
                email: ''
            },
            street: '',
            city: 'loading...',
            businessHours: {
                weekday: 'loading...',
                saturday: 'loading...',
                sunday: 'loading...'
            }
        };

        this.toggleEditTooltip = this.toggleEditTooltip.bind(this);
        this.toggleEditPasswordTooltip = this.toggleEditPasswordTooltip.bind(this);

        axios.get(`/api/users/${props.username}`)
            .then(res => {
                this.setState({
                    ...res.data,
                    color: res.data.isEnterprise ? 'premium-user' : 'ordinary-user'
                });
            }).catch(() => {
                window.location.href = '/notfound';
            });
    }

    toggleEditTooltip() {
        this.setState({
            editTooltipOpen: !this.state.editTooltipOpen
        })
    };

    toggleEditPasswordTooltip() {
        this.setState({
            editPasswordTooltipOpen: !this.state.editPasswordTooltipOpen
        })
    }

    render() {
        return (
            <Container className='container-info mb-5'>
                <Row>
                    <Col xs={5}>
                        {
                            this.state.isEnterprise === true ?
                                <img src='https://image.flaticon.com/icons/svg/265/265667.svg' alt='IMG' /> :
                                <img src='https://image.flaticon.com/icons/svg/265/265674.svg' alt='IMG' />
                        }
                        {
                            sessionStorage.getItem('username') === this.state.username ?
                                <div className="editUserDiv">
                                    <a href='/edit/info'><img style={{ "width": "25%", "marginRight": "20px" }} id="editUserLink" src="https://image.flaticon.com/icons/svg/148/148926.svg" alt="I"></img></a>
                                    <Tooltip toggle={this.toggleEditTooltip} placement="left" isOpen={this.state.editTooltipOpen} target="editUserLink">Izmijeni nalog</Tooltip>
                                    <a href='/edit/passwd'><img style={{ "width": "25%", "transform": "scaleX(-1)" }} id="editUserPasswordLink" src="https://image.flaticon.com/icons/svg/179/179543.svg" alt="I"></img></a>
                                    <Tooltip toggle={this.toggleEditPasswordTooltip} placement="right" isOpen={this.state.editPasswordTooltipOpen} target="editUserPasswordLink">Izmijeni lozinku</Tooltip>
                                </div> : null
                        }

                    </Col>
                    <Col xs>
                        OSNOVNE INFORMACIJE
                        <InputGroup size='sm'>
                            <InputGroupAddon addonType='prepend'>KORISNIČKO IME</InputGroupAddon>
                            <Input disabled className={this.state.color} value={this.state.username} />
                        </InputGroup>
                        <InputGroup size='sm'>
                            <InputGroupAddon addonType='prepend'>IME</InputGroupAddon>
                            <Input disabled className={this.state.color} value={this.state.name} />
                        </InputGroup>
                        <InputGroup size='sm'>
                            <InputGroupAddon addonType='prepend'>PREZIME</InputGroupAddon>
                            <Input disabled className={this.state.color} value={this.state.surname} />
                        </InputGroup>
                        <InputGroup size='sm' hidden={this.state.contact.email === ''}>
                            <InputGroupAddon addonType='prepend'>E-MAIL</InputGroupAddon>
                            <Input disabled className={this.state.color} value={this.state.contact.email} />
                        </InputGroup>
                        <InputGroup size='sm' hidden={this.state.contact.phone === ''}>
                            <InputGroupAddon addonType='prepend'>TELEFON</InputGroupAddon>
                            <Input disabled className={this.state.color} value={this.state.contact.phone} />
                        </InputGroup>
                        <InputGroup size='sm' hidden={this.state.contact.website === ''}>
                            <InputGroupAddon addonType='prepend'>WEB-SAJT</InputGroupAddon>
                            <Input disabled className={this.state.color} value={this.state.contact.website} />
                        </InputGroup>
                        <InputGroup size='sm' hidden={this.state.street === ''}>
                            <InputGroupAddon addonType='prepend'>ADRESA</InputGroupAddon>
                            <Input disabled className={this.state.color} value={this.state.street} />
                        </InputGroup>
                        <InputGroup size='sm' className='mb-5'>
                            <InputGroupAddon addonType='prepend'>GRAD</InputGroupAddon>
                            <Input disabled className={this.state.color} value={this.state.city} />
                        </InputGroup>
                        RADNO VRIJEME
                        <InputGroup size='sm'>
                            <InputGroupAddon addonType='prepend'>RADNIM DANOM</InputGroupAddon>
                            <Input disabled className={this.state.color} value={this.state.businessHours.weekday} />
                        </InputGroup>
                        <InputGroup size='sm'>
                            <InputGroupAddon addonType='prepend'>SUBOTOM</InputGroupAddon>
                            <Input disabled className={this.state.color} value={this.state.businessHours.saturday} />
                        </InputGroup>
                        <InputGroup size='sm'>
                            <InputGroupAddon addonType='prepend'>NEDJELJOM</InputGroupAddon>
                            <Input disabled className={this.state.color} value={this.state.businessHours.sunday} />
                        </InputGroup>
                    </Col>
                </Row>
            </Container>
        );
    }
}