import React, {
    Component,
} from 'react';

import {
    Container
} from 'reactstrap';

export default class AppFooter extends Component {

    render() {
        return (
            <Container className='footer'>
                <div className="footer-div">
                    <h3>Posjetite nas:</h3>
                    <div className="footer-icons">
                        <a href="/"><img src="https://image.flaticon.com/icons/svg/174/174848.svg" alt="fb" /></a>
                        <a href="/"><img src="https://image.flaticon.com/icons/svg/174/174855.svg" alt="ig" /></a>
                        <a href="https://gitlab.com/is2018/serve-it"><img src="https://image.flaticon.com/icons/svg/24/24233.svg" alt="gl" /></a>
                    </div>
                </div>
                <div className="footer-div" />
                <div className="footer-div">
                    <h3>Kontaktirajte nas:</h3>
                    <p>E-mail: <a href="/">support@serve.it</a></p>
                    <p>Telefon: <span className="home-info">+387 65 557 921</span></p>
                    <p>Adresa: <span className="home-info">Patre 5, Banja Luka</span></p>
                    <br />
                </div>
            </Container>
        );
    }
}