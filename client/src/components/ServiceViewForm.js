import React, {
    Component,
    Fragment
} from 'react';
import {
    Button,
    Col,
    Collapse,
    Container,
    Input,
    Jumbotron,
    ListGroup,
    ListGroupItem,
    Row
} from 'reactstrap';
import SearchBar from './SearchBar';
import ComplaintModal from './ComplaintModal';
import axios from 'axios';
import icons from '../static_data/icons';

export default class ServiceViewForm extends Component {

    state = {
        owner: '',
        category: '',
        title: '',
        description: '',
        rating: {
            positive: 0,
            negative: 0
        },
        comments: [],
        collapse: false
    }

    commentField = '';

    componentWillMount() {
        if (window.location.pathname.length !== 34) {
            window.location.pathname = '/notfound';
        }
        axios.get(`/api${window.location.pathname}`)
            .then(res => {
                if (res.data) {
                    if (res.data.description) {
                        res.data.description = res.data.description.replace(/\n/g, '<br />');
                    }
                    this.setState(res.data);
                } else {
                    window.location.pathname = '/notfound';
                }
            }).catch(() => window.location.pathname = '/notfound');
    }

    writeCommentClick = () => {
        if (this.commentField === '') {
            this.setState({
                collapse: !this.state.collapse
            });
            setTimeout(() => {
                document.getElementById('commentField').focus();
            }, 250);
        } else {
            axios.post(`/api/services/${this.state._id}/comment`, {
                comment: this.commentField
            }, {
                    headers: {
                        authorization: `Bearer ${sessionStorage.getItem('Bearer')}`
                    }
                }).then(() => {
                    this.state.comments.unshift({
                        author: sessionStorage.getItem('username'),
                        date: Date.now(),
                        body: this.commentField
                    })
                    this.commentField = '';
                    document.getElementById('commentField').value = '';
                    window.location.hash = 'COMMENTS';
                    this.writeCommentClick();
                });
        }
    }

    render() {
        return (
            <Container>
                <SearchBar />
                <br /><br />
                <Container className='container-info mb-5'>
                    <Row>
                        <Col xs={4}>
                            <img src={icons[this.state.category]}
                                width='100%' height='100%' alt='IMG' />
                        </Col>
                        <Col xs>
                            <Row>
                                <Col xs={8}>
                                    <h2 className='d-flex justify-content-center mt-3'>
                                        {this.state.title}
                                    </h2>
                                    <h4 className='d-flex justify-content-center mt-3'>
                                        {this.state.category}
                                    </h4>
                                </Col>
                                <Col xs={3}>
                                    <a href={`/users/${this.state.owner}`} className='href'>
                                        <img src='https://image.flaticon.com/icons/svg/236/236817.svg' alt='' />
                                        <div className='d-flex justify-content-center'>
                                            {this.state.owner}
                                        </div>
                                    </a>
                                </Col>
                            </Row>
                            <Row className='mt-5 ml-5'>
                                <Col xs={2}>
                                    <img src='https://image.flaticon.com/icons/svg/996/996571.svg'
                                        style={{ transform: 'scaleY(-1)' }} alt='LIKE' onClick={() => {
                                            if (sessionStorage.getItem('username') === this.state.owner) {
                                                return;
                                            }
                                            if (!sessionStorage.getItem('Bearer')) {
                                                document.getElementById('login-btn').click();
                                                return;
                                            }
                                            axios.post('/api/likes/like', {
                                                service: this.state._id
                                            }, {
                                                    headers: {
                                                        authorization: 'Bearer ' + sessionStorage.getItem('Bearer')
                                                    }
                                                }).then(() => {
                                                    this.setState({
                                                        rating: {
                                                            positive: this.state.rating.positive + 1,
                                                            negative: this.state.rating.negative
                                                        }
                                                    });
                                                }).catch(() => console.log('App is down'));
                                        }
                                        } className='hand' />
                                </Col>
                                <Col xs={4} style={{ fontSize: 42, color: 'green' }}>
                                    {this.state.rating.positive}
                                </Col>
                                <Col xs={2}>
                                    <img src='https://image.flaticon.com/icons/svg/996/996571.svg' alt='DISLIKE'
                                        onClick={() => {
                                            if (sessionStorage.getItem('username') === this.state.owner) {
                                                return;
                                            }
                                            if (!sessionStorage.getItem('Bearer')) {
                                                document.getElementById('login-btn').click();
                                                return;
                                            }
                                            axios.post('/api/likes/dislike', {
                                                service: this.state._id
                                            }, {
                                                    headers: {
                                                        authorization: 'Bearer ' + sessionStorage.getItem('Bearer')
                                                    }
                                                }).then(() => {
                                                    this.setState({
                                                        rating: {
                                                            positive: this.state.rating.positive,
                                                            negative: this.state.rating.negative + 1
                                                        }
                                                    });
                                                }).catch(() => console.log('App is down'));
                                        }
                                        } className='hand' />
                                </Col>
                                <Col xs={2} style={{ fontSize: 42, color: 'red' }}>
                                    {this.state.rating.negative}
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Jumbotron className='mt-5 border-top border-warning rounded'>
                        <h5 className='d-flex justify-content-center mb-5'>
                            OPIS
                    </h5>
                        <p dangerouslySetInnerHTML={{ __html: this.state.description }} />
                    </Jumbotron>
                    <Jumbotron className='mt-5 border-top border-warning rounded'>
                        <h5 className='mb-5' id='COMMENTS'>
                            KOMENTARI
                        </h5>
                        <hr />
                        {
                            sessionStorage.getItem('Bearer') && sessionStorage.getItem('username') !== this.state.owner ?
                                <Fragment>
                                    <Collapse isOpen={this.state.collapse}>
                                        <Input id='commentField' bsSize='sm' type='textarea' onChange={(e) => {
                                            this.commentField = e.target.value;
                                        }} className='mb-1' />
                                    </Collapse>
                                    <Row className='mb-3'>
                                        <Col xs>
                                            <Button outline size='sm' color='primary' onClick={this.writeCommentClick}>
                                                OSTAVI KOMENTAR</Button>
                                        </Col>
                                        <Col xs>
                                            <ComplaintModal service={window.location.pathname.split('/')[2]} />
                                        </Col>
                                    </Row>
                                </Fragment> : null
                        }
                        <ListGroup flush>
                            {this.state.comments.map(({ author, body, date }) => (
                                <ListGroupItem key={date} className='border-top border-warning rounded'>
                                    <Row className='mb-2 outline text-primary'>
                                        <Col xs={9}>
                                            <a href={`/users/${author}`}>{author}</a>
                                        </Col>
                                        <Col xs>
                                            {
                                                new Date(date).toLocaleDateString('bs-BA')
                                            }
                                        </Col>
                                    </Row>
                                    {body}
                                </ListGroupItem>
                            ))}
                        </ListGroup>
                    </Jumbotron>
                </Container >
            </Container>
        );
    }
}