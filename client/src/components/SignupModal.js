import React, {
    Component
} from 'react';
import {
    Alert,
    Button,
    Container,
    Form,
    FormFeedback,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalHeader,
    ModalBody,
    NavLink,
    NavItem,
    Spinner
} from 'reactstrap';
import cities from '../static_data/cities';
import axios from 'axios';

export default class SignupModal extends Component {
    state = {
        isOpen: false,
        alert: false,
        success: false,
        clicked: false,
        invalidPass: false
    }

    account = {
        city: cities[0],
        contact: {}
    }

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    onChange = (e) => {
        this.account[e.target.id] = e.target.value
    }

    onContactChange = (e) => {
        this.account.contact[e.target.id] = e.target.value;
    }

    onSubmit = (e) => {
        e.preventDefault();
        if (!this.account.password ||
            this.account.password !== this.account.password_verify) {
            this.setState({
                invalidPass: true,
                alert: false
            });
            return;
        }
        this.setState({
            alert: false,
            clicked: true,
            invalidPass: false
        });

        axios.post('/api/users/new', this.account)
            .then(res => {
                if (res.status === 200) {
                    this.setState({
                        success: true
                    });
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                }
            }).catch(_err => {
                this.setState({
                    alert: true,
                    success: false,
                    clicked: false
                });
            });
    }

    render() {
        return (
            <div>
                <NavItem className='hand'>
                    <NavLink onClick={this.toggle}>KREIRAJ NALOG</NavLink>
                </NavItem>
                <Modal isOpen={this.state.isOpen} toggle={this.toggle} autoFocus={false}>
                    <ModalHeader toggle={this.toggle}>KREIRAJ NALOG</ModalHeader>
                    <ModalBody>
                        <Container className='container-form'>
                            <Form onSubmit={this.onSubmit}>
                                <FormGroup>
                                    <Label size='sm' for='username'>Korisničko ime <span style={{ "color": "red" }}>*</span></Label>
                                    <Input bsSize='sm' id='username' disabled={this.state.clicked} onChange={this.onChange} autoFocus />
                                </FormGroup>
                                <FormGroup>
                                    <Label size='sm' for='name'>Ime <span style={{ "color": "red" }}>*</span></Label>
                                    <Input bsSize='sm' id='name' disabled={this.state.clicked} onChange={this.onChange} />
                                </FormGroup>
                                <FormGroup>
                                    <Label size='sm' for='surname'>Prezime <span style={{ "color": "red" }}>*</span></Label>
                                    <Input bsSize='sm' id='surname' disabled={this.state.clicked} onChange={this.onChange} />
                                </FormGroup>
                                <FormGroup>
                                    <Label size='sm' for='street'>Adresa (opciono)</Label>
                                    <Input bsSize='sm' id='street' disabled={this.state.clicked} onChange={this.onChange} />
                                </FormGroup>
                                <FormGroup>
                                    <Label size='sm' for='city'>Grad <span style={{ "color": "red" }}>*</span></Label>
                                    <Input bsSize='sm' type='select' id='city' disabled={this.state.clicked} onChange={this.onChange} >
                                        {cities.map((city) => (<option key={city}>{city}</option>))}
                                    </Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label size='sm' for='email'>E-mail (opciono)</Label>
                                    <Input bsSize='sm' type='email' id='email' disabled={this.state.clicked} onChange={this.onContactChange} />
                                </FormGroup>
                                <FormGroup>
                                    <Label size='sm' for='phone'>Telefon (opciono)</Label>
                                    <Input bsSize='sm' id='phone' disabled={this.state.clicked} onChange={this.onContactChange} />
                                </FormGroup>
                                <FormGroup>
                                    <Label size='sm' for='website'>Websajt (opciono)</Label>
                                    <Input bsSize='sm' id='website' disabled={this.state.clicked} onChange={this.onContactChange} />
                                </FormGroup>
                                <FormGroup>
                                    <Label size='sm' for='password'>Lozinka <span style={{ "color": "red" }}>*</span></Label>
                                    <Input bsSize='sm' type='password' id='password' disabled={this.state.clicked}
                                        invalid={this.state.invalidPass} onChange={this.onChange} />
                                    <FormFeedback>Lozinke se ne podudaraju</FormFeedback>
                                </FormGroup>
                                <FormGroup>
                                    <Label size='sm' for='password_verify'>Potvrdi lozinku <span style={{ "color": "red" }}>*</span></Label>
                                    <Input bsSize='sm' type='password' id='password_verify' disabled={this.state.clicked}
                                        invalid={this.state.invalidPass} onChange={this.onChange} />
                                    <FormFeedback>Lozinke se ne podudaraju</FormFeedback>
                                </FormGroup>
                                <FormGroup>
                                    <Alert color='danger' hidden={!this.state.alert}>
                                        Popunite sva tražena polja
                                </Alert>
                                    <Alert color='success' hidden={!this.state.success}>
                                        Nalog uspješno kreiran
                                </Alert>
                                </FormGroup>
                                <FormGroup>
                                    {this.state.clicked ?
                                        <div className='d-flex justify-content-center'>
                                            <Spinner color='warning' />
                                        </div> :
                                        <Button outline block color='warning' size='sm'>KREIRAJ NALOG</Button>
                                    }
                                </FormGroup>
                            </Form>
                        </Container>
                    </ModalBody>
                </Modal>
            </div>
        );
    }
}