import React, {
    Component
} from 'react';

import {
    Container,
    Table,
    Button,
    Row,
    Col
} from 'reactstrap';

import axios from 'axios';

export default class AdminUsers extends Component {

    state = {
        users: [],
        visibleUsers: [],
        loaded: false,
        updated: false,
        contentMultiplier: 1,
        usersPerLoad: 20
    }

    constructor(props) {
        super(props);
        this.filterUsers = this.filterUsers.bind(this);
        this.increaseContentMultiplier = this.increaseContentMultiplier.bind(this);
    }

    componentWillMount() {
        axios.get('/api/users/all').then(res => {
            this.setState({
                loaded: true,
                users: res.data,
                visibleUsers: res.data
            });
        }).catch(() => console.log('Cant get users'));
    }

    filterUsers() {
        this.setState({
            contentMultiplier: 1,
            visibleUsers: this.state.users
        }, () => {
            //filtering by username
            this.setState({
                visibleUsers: this.state.visibleUsers.filter(user => user.username.includes(document.getElementById('username-filter').value))
            }, () => {
                //filtering by checkboxes
                //a quick workaround to solve sync problems
                //using ternary operator instead of if statement
                this.setState({
                    visibleUsers: document.getElementById('active-cb').checked ? this.state.visibleUsers.filter((user) => user.active === true) : this.state.visibleUsers
                }, () => {
                    //other checkbox
                    this.setState({
                        visibleUsers: document.getElementById('premium-cb').checked ? this.state.visibleUsers.filter((user) => user.isEnterprise === true) : this.state.visibleUsers
                    })
                })
            })
        })
    }

    increaseContentMultiplier() {
        this.setState({
            contentMultiplier: this.state.contentMultiplier + 1
        });
    }

    //refactor this piece of shit code when you have time lol
    toggleActive = (event) => {
        var id = event.target.id;
        var user = this.state.users.filter(user => user.username === id)[0];
        var oldActiveState = user.active;

        axios.put('/api/users/toggleActive', user, {
            headers: {
                authorization: 'Bearer ' + sessionStorage.getItem('AdminBearer')
            }
        }).then(res => {
            if (res.status === 200) {
                this.state.users.filter(user => user.username === id)[0].active = !oldActiveState;
                this.setState({
                    success: true,
                    users: this.state.users
                });
            }
        }).catch((_err) => console.log('error toggling active state'));
    }

    togglePremium = (event) => {
        var id = event.target.id;
        var user = this.state.users.filter(user => user.username === id)[0];
        var oldPremiumStatus = user.isEnterprise;

        axios.put('/api/users/togglePremium', user, {
            headers: {
                authorization: 'Bearer ' + sessionStorage.getItem('AdminBearer')
            }
        }).then(res => {
            if (res.status === 200) {
                this.state.users.filter(user => user.username === id)[0].isEnterprise = !oldPremiumStatus;
                this.setState({
                    success: true,
                    users: this.state.users
                });
            }
        }).catch((_err) => console.log('error toggling active state'));
    }


    render() {
        return (
            <Container>
                <Row className="admin-users-toolbar">
                    <Col xs={4}><input style={{ "width": "100%" }} id="username-filter" type="text" onChange={this.filterUsers} placeholder="Pronađite korisnika..."></input></Col>
                    <Col xs={4}><input id="active-cb" type="checkbox" onChange={this.filterUsers}></input><label>Samo aktivni korisnici</label></Col>
                    <Col xs={4}><input id="premium-cb" type="checkbox" onChange={this.filterUsers}></input><label>Samo premium korisnici</label></Col>
                </Row>
                <Table>
                    <thead>
                        <tr>
                            <th>Korisničko ime</th>
                            <th>Ime</th>
                            <th>Prezime</th>
                            <th>Premium</th>
                            <th>Aktivan</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.visibleUsers.slice(0, this.state.contentMultiplier * this.state.usersPerLoad).map(({ name, surname, username, isEnterprise, active }) => (
                                <tr>
                                    <td>{username}</td>
                                    <td>{name}</td>
                                    <td>{surname}</td>
                                    <td>{isEnterprise ? 'Da' : 'Ne'}</td>
                                    <td>{active ? 'Da' : 'Ne'}</td>
                                    <td><Button id={username} color='warning' onClick={this.toggleActive}>{active ? 'Deaktiviraj' : 'Aktiviraj'}</Button></td>
                                    <td><Button id={username} color='warning' onClick={this.togglePremium}>{isEnterprise ? 'Ukloni Premium status' : 'Stavi premium status'}</Button></td>
                                </tr>
                            ))
                        }
                    </tbody>
                </Table>
                <hr />
                <div style={{ "textAlign": "center", "width": "100%" }}>
                    {
                        this.state.contentMultiplier * this.state.usersPerLoad < this.state.visibleUsers.length ?
                            <Button color='link' style={{ "cursor": "pointer", "color": "orange" }} onClick={this.increaseContentMultiplier}>Učitaj još korisnika</Button> :
                            <p>Ucitali ste sve korsinike</p>
                    }
                </div>
            </Container>
        )
    }
}