import React, {
    Component
} from 'react';

import {
    Container
}from 'reactstrap';
import AdminNavbar from './AdminNavbar';
import AdminServices from './AdminServices';

export default class AdminUsers extends Component{

    render() {
        return(
            <Container>
                <AdminNavbar/>
                <AdminServices/>
            </Container>
        )
    }
}