import React, {
    Component
} from 'react';

import {
    Container,
    Form,
    FormGroup,
    Button,
    Input,
    Alert,
    Label,
    Spinner
} from 'reactstrap';

const axios = require('axios');


export default class AdminLoginForm extends Component {

    state = {
        alert: false,
        clicked: false
    };

    credentials = {}

    onChange = (e) => {
        this.credentials[e.target.id] = e.target.value
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.setState({
            alert: false,
            clicked: true
        });

        axios.post('/api/admin', this.credentials)
            .then(res => {
                if (typeof res.data === 'string') {
                    console.log('strign eee');
                    sessionStorage.setItem('AdminBearer', res.data);
                    sessionStorage.setItem('adminUsername', this.credentials.username);
                    window.location.reload();
                } else {
                    this.setState({
                        alert: true,
                        clicked: false
                    });
                }
            }).catch(_err => {
                this.setState({
                    alert: true,
                    clicked: false
                });
            });
    }


    render() {
        return (
            <Container className="adminLoginContainer">
                <div className="adminPanel">
                    <img src="/sit-logo.png" width="80%" alt=""></img><br /><br />
                </div>
                <Form onSubmit={this.onSubmit}>
                    <FormGroup>
                        <Label for="username">Korisničko ime</Label>
                        <Input id="username" disabled={this.state.clicked} onChange={this.onChange} autoFocus></Input>
                    </FormGroup>
                    <FormGroup>
                        <Label for="password">Lozinka</Label>
                        <Input id="password" type='password' disabled={this.state.clicked} onChange={this.onChange}></Input>
                    </FormGroup>
                    <FormGroup>
                        <Alert color='danger' hidden={!this.state.alert}>
                            Kredencijali za prijavu nisu tačni.
                            </Alert>
                    </FormGroup>
                    <FormGroup>
                        {this.state.clicked ?
                            <div className='d-flex justify-content-center'>
                                <Spinner color='warning' />
                            </div> :
                            <Button outline color='warning' block>PRIJAVI SE</Button>
                        }
                    </FormGroup>
                </Form>
            </Container>
        )
    };
}