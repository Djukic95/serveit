import React, {
    Component
} from 'react';

import {
    Container,
    Navbar,
    NavbarBrand,
    NavItem,
    NavLink,
    Nav,
    Row,
    Col
} from 'reactstrap';

export default class AdminNavbar extends Component {

    logout = () => {
        sessionStorage.clear();
        window.location.href = '/admin';
    }

    render() {
        return (
            <Container>
                <Navbar color='light' light>
                    <NavbarBrand href="/"><img src="/sit-logo.png" alt="ADMIN"></img></NavbarBrand>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink onClick={this.logout} href="">ODJAVI SE</NavLink>
                        </NavItem>
                    </Nav>
                </Navbar>
                <Row className="admin-options" >
                    <Col xs={4}>
                        <a href="/admin/statistics">STATISTIKA</a>
                    </Col>
                    <Col xs={4}>
                        <a href="/admin/users">KORISNICI</a>
                    </Col>
                    <Col xs={4}>
                        <a href="/admin/services">USLUGE</a>
                    </Col>
                </Row>
            </Container>
        );
    };
}