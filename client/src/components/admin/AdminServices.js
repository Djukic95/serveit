import React, {
    Component
} from 'react';

import {
    Container,
    Table,
    Button,
    Modal,
    ModalBody,
    ModalHeader,
    Row,
    Col
} from 'reactstrap';

import axios from 'axios'

export default class AdminServices extends Component {

    state = {
        services: [],
        visibleServices: [],
        complaints: [],
        servicesWithComplaints: [],
        modalComplaints: [],
        loaded: false,
        updated: false,
        modal: false,
        contentMultiplier: 1,
        servicesPerLoad: 20
    }

    constructor(props) {
        super(props);
        this.filterServices = this.filterServices.bind(this);
        this.toggle = this.toggle.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.increaseContentMultiplier = this.increaseContentMultiplier.bind(this);

    }


    componentWillMount() {
        axios.get('/api/services/all').then(res => {
            this.setState({
                services: res.data,
                visibleServices: res.data,
                loaded: true
            }, () => {
                //callback to get complaints for services
                axios.get('/api/complaints/all').then(res2 => {
                    this.setState({
                        complaints: res2.data,

                    }, () => {
                        this.setState({
                            servicesWithComplaints: this.state.visibleServices.filter((service) =>
                                res2.data.filter(complaint => complaint.service === service._id).length !== 0)
                        })
                    });
                }).catch(() => console.log('Cant get complaints'));
            });
        }).catch(() => console.log('Cant get services'));

    }

    filterServices() {
        this.setState({
            contentMultiplier: 1,
            visibleServices: this.state.services
        }, () => {
            //filtering by checkboxes
            //a quick workaround to solve sync problems
            //using ternary operator instead of if statement
            this.setState({
                visibleServices: document.getElementById('active-cb').checked ? this.state.visibleServices.filter((service) => service.active === true) : this.state.visibleServices
            }, () => {
                //other checkbox
                this.setState({
                    visibleServices: document.getElementById('withComplaint-cb').checked ?
                        (document.getElementById('active-cb').checked ? this.state.servicesWithComplaints.filter((service) => service.active === true) : this.state.servicesWithComplaints) :
                        this.state.visibleServices
                }, () => {
                    this.setState({
                        visibleServices: this.state.visibleServices.filter(service => service.owner.includes(document.getElementById('username-filter').value))
                    })
                })
            })
        })
    }


    //refactor this piece of shit code when you have time lol drugi put(kopiro od jovic_brt-a)
    toggleActive = (event) => {

        var id = event.target.id;

        var service = this.state.services.filter(service => service._id === id)[0];
        var oldActiveState = service.active;

        axios.put('/api/services/toggleActive', service, {
            headers: {
                authorization: 'Bearer ' + sessionStorage.getItem('AdminBearer')
            }
        }).then(res => {
            if (res.status === 200) {
                this.state.visibleServices.filter(service => service._id === id)[0].active = !oldActiveState;
                this.setState({
                    success: true,
                    service: this.state.service
                });
            }
        }).catch((_err) => console.log('error toggling active state'));
    }

    viewComplaints = (event) => {

        var id = event.target.id;
        this.setState({
            modalComplaints: this.state.complaints.filter(complaint => complaint.service === id)
        });
        this.toggle()
    }

    toggle() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }

    toggleModal() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }

    increaseContentMultiplier() {
        this.setState({
            contentMultiplier: this.state.contentMultiplier + 1
        });
    }


    render() {
        return (

            <Container>

                <Modal isOpen={this.state.modal} toggle={this.toggleModal} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Žalbe</ModalHeader>
                    <ModalBody>
                        <Table>
                            <thead>
                                <tr>
                                    <th>Korisnik </th>
                                    <th>Razlog žalbe </th>
                                    <th>Datum</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.modalComplaints.map(({ user, reason, date }) => (
                                        <tr>
                                            <td>{user}</td>
                                            <td>{reason}</td>
                                            <td>{new Date(Date.parse(date)).toLocaleString("sr")}</td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </Table>
                    </ModalBody>
                </Modal>

                <Row className="admin-users-toolbar">
                    <Col xs={4}><input style={{ "width": "100%" }} id="username-filter" type="text" onChange={this.filterServices} placeholder="Pronađite usluge za korisnika..."></input></Col>
                    <Col xs={4}><input id="active-cb" type="checkbox" onChange={this.filterServices}></input><label>Samo aktivne usluge</label></Col>
                    <Col xs={4}><input id="withComplaint-cb" type="checkbox" onChange={this.filterServices}></input><label>Samo usluge sa žalbom</label></Col>
                </Row>

                <Table>
                    <thead>
                        <tr>
                            <th>Usluga</th>
                            <th>Kategorija</th>
                            <th>Ponuđač</th>
                            <th>Pozitivnih dojmova</th>
                            <th>Negativnih dojmova</th>
                            <th>Broj žalbi</th>
                            <th>Aktivno </th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {

                            this.state.visibleServices.slice(0, this.state.servicesPerLoad * this.state.contentMultiplier).map(({ title, category, owner, rating, active, _id }) => (
                                <tr>
                                    <td>{title}</td>
                                    <td>{category}</td>
                                    <td>{owner}</td>
                                    <td>{rating.positive}</td>
                                    <td>{rating.negative}</td>
                                    <td>{this.state.complaints.filter(complaint => complaint.service === _id).length}</td>
                                    <td>{active ? 'Da' : 'Ne'}</td>
                                    <td><Button id={_id} color='warning' onClick={this.toggleActive}>{active ? 'Deaktiviraj' : 'Aktiviraj'}</Button></td>
                                    <td><Button id={_id} color='warning' onClick={this.viewComplaints}>Pregledaj žalbe</Button></td>
                                </tr>
                            ))
                        }
                    </tbody>
                </Table>
                <hr />
                <div style={{ "textAlign": "center", "width": "100%" }}>
                    {
                        this.state.contentMultiplier * this.state.servicesPerLoad < this.state.visibleServices.length ?
                            <Button color='link' style={{ "cursor": "pointer", "color": "orange" }} onClick={this.increaseContentMultiplier}>Učitaj još usluga</Button> :
                            <p>Ucitali ste sve usluge</p>
                    }
                </div>
            </Container>
        )
    }
}