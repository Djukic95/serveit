import React, {
    Component
} from 'react';

import {
    Container
} from 'reactstrap';


import axios from 'axios';


export default class AdminStatistics extends Component {

    constructor(props){
        super(props);

        this.numberOfServices = this.numberOfServices.bind(this);
        this.numberOfUsers = this.numberOfUsers.bind(this);
    }

    state = {
        services: [],
        users: [],
        cityCounter: []
    }

    componentWillMount() {
        axios.get('/api/users/all').then(res => {
            this.setState({
                users: res.data
            }, () =>{
                //callback to count the cities for the users
                var cityCounterMap = new Map();
                this.state.users.map(user => user.city).forEach(city => {
                    if(cityCounterMap.get(city) === undefined){
                        cityCounterMap.set(city, 1);
                    }else{
                        cityCounterMap.set(city, cityCounterMap.get(city) + 1);
                    }
                })
                this.setState({
                    cityCounter: Array.from(cityCounterMap).sort((a, b) => b[1] - a[1])
                }, () => {console.log(this.state.cityCounter)})
            });
        }).catch(() => console.log('Cant get users'));

        axios.get('/api/services/all').then(res => {
            this.setState({
                services: res.data
            });
        }).catch(() => console.log('Cant get services'));

    }

    userTimeFilter(user, timeMillis) {
        return Date.now() - (new Date(user.created).getTime()) < timeMillis;
    }

    serviceTimeFilter(service, timeMillis) {
        return Date.now() - (new Date(service.date).getTime()) < timeMillis;
    }

    numberOfServices(category){
        return this.state.services.filter(service => service.category === category).length;
    }

    numberOfUsers(isEnterprise){
        return this.state.users.filter(user => user.isEnterprise === isEnterprise).length;
    }

    //nameOrCount: 0 for name, 1 for count
    cityCounterInfo(cityIndex, nameOrCount){
        return this.state.cityCounter[cityIndex] !== undefined?this.state.cityCounter[cityIndex][nameOrCount]:'';
    }

    render() {
        return (
            <Container>
                <div className="user-stats-body">
                    <div className="user-stats-img">
                        <img src="https://image.flaticon.com/icons/svg/236/236822.svg" alt=""></img>
                        <label>Korisnička statistika</label>
                    </div>
                    <div className="user-stats-data">
                        <div className="user-stats-card">
                            <h4 style={{ "color": "#e7eff6" }}>Registrovani korisnici</h4>
                            <hr />
                            <p> ZADNJA 24 SATA</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.state.users.filter(user => this.userTimeFilter(user, 86400000)).length}</p>
                            <p>ZADNJIH 7 DANA</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.state.users.filter(user => this.userTimeFilter(user, 86400000 * 7)).length}</p>
                            <p> ZADNJIH 30 DANA</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.state.users.filter(user => this.userTimeFilter(user, 86400000 * 30)).length}</p>
                        </div>
                        <div className="user-stats-card">
                            <h4 style={{ "color": "#e7eff6" }}>Podjela korisnika</h4>
                            <br></br>
                            <hr/>
                            <p>UKUPNO KORISNIKA</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.state.users.length}</p>
                            <p>OBIČNIH KORISNIKA</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.numberOfUsers(false)}</p>
                            <p>PREMIUM KORISNIKA</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.numberOfUsers(true)}</p>
                        </div>
                        <div className="user-stats-card">
                            <h4 style={{ "color": "#e7eff6" }}>Najpopularniji gradovi</h4>
                            <hr/>
                            <p>{this.cityCounterInfo(0, 0)}</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.cityCounterInfo(0, 1)}</p>
                            <p>{this.cityCounterInfo(1, 0)}</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.cityCounterInfo(1, 1)}</p>
                            <p>{this.cityCounterInfo(2, 0)}</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.cityCounterInfo(2, 1)}</p>
                        </div>
                    </div>
                </div>
                <hr />
                <div className="user-stats-body">
                    <div className="user-stats-img">
                        <img src="https://image.flaticon.com/icons/svg/164/164426.svg" alt=""></img>
                        <label>Statistika usluga</label>
                    </div>
                    <div className="user-stats-data">
                        <div className="user-stats-card">
                            <h4 style={{ "color": "#e7eff6" }}>Kreirane usluge</h4>
                            <br></br>
                            <hr />
                            <p> ZADNJA 24 SATA</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.state.services.filter(service => this.serviceTimeFilter(service, 86400000)).length}</p>
                            <p>ZADNJIH 7 DANA</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.state.services.filter(service => this.serviceTimeFilter(service, 86400000 * 7)).length}</p>
                            <p> ZADNJIH 30 DANA</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.state.services.filter(service => this.serviceTimeFilter(service, 86400000 * 30)).length}</p>
                        </div>
                        <div className="user-stats-card">
                            <h4 style={{ "color": "#e7eff6" }}>Usluge po kategorijama</h4>
                            <hr />
                            <p>KUĆA I KANCELARIJA</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.numberOfServices('Kuća i kancelarija')}</p>
                            <p>BIZNIS</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.numberOfServices('Biznis')}</p>
                            <p>EDUKACIJA</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.numberOfServices('Edukacija')}</p>
                        </div>
                        <div className="user-stats-card">
                            <h4 style={{ "color": "#e7eff6" }}>Usluge po kategorijama</h4>
                            <hr/>
                            <p>VOZILA</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.numberOfServices('Vozila')}</p>
                            <p>LJEPOTA I ZDRAVLJE</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.numberOfServices('Ljepota i zdravlje')}</p>
                            <p>OSTALO</p>
                            <p style={{ "color": "orange", "fontSize": "25px", "fontWeight": "bold" }}>{this.numberOfServices('Ostalo')}</p>
                        </div>
                    </div>
                </div>
                <div className="more-service-stats">
                Broj usluga po korisniku: <span style={{"color":"orange"}}>{(this.state.services.length/this.state.users.length).toFixed(2)}</span>
                </div>
            </Container>

        )
    }
}