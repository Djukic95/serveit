import React, {
    Component
} from 'react';

import {
    Container
}from 'reactstrap';
import AdminNavbar from './AdminNavbar';
import AdminUsers from './AdminUsers';

export default class AdminUsersForm extends Component{

    render() {
        return(
            <Container>
                <AdminNavbar/>
                <AdminUsers/>
            </Container>
        )
    }
}