import React, {
    Component
} from 'react';

import {
    Container
} from 'reactstrap';

import AdminNavbar from './AdminNavbar';
import AdminStatistics from './AdminStatistics';

export default class AdminHomeForm extends Component{

    render(){
        return(
            <Container>
                <AdminNavbar/>
                <AdminStatistics/>
            </Container>
        );
    };
}