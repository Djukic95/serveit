import React, {
    Component
} from 'react';
import {
    Alert,
    Button,
    Container,
    Badge,
    Spinner
} from 'reactstrap';
import axios from 'axios';
import SearchBar from './SearchBar';
import icons from '../static_data/icons';

export default class ServiceListForm extends Component {

    state = {
        isOpen: false,
        loaded: false,
        hasMore: true,
        services: []
    }

    page = 1;

    componentWillMount() {
        this.searchUrl = new URL(window.location.href);
        this.searchUrl.pathname = '/api/services';
        this.searchUrl.searchParams.set('page', this.page);
        axios.get(this.searchUrl)
            .then(res => {
                this.setState({
                    loaded: true,
                    services: res.data
                }, () => {
                    for(var index in this.state.services){
                        this.isUserPremuim(this.state.services[index]._id, this.state.services[index].owner)
                    }
                });
                if (res.data.length < 10) {
                    this.setState({
                        hasMore: false
                    });
                    document.getElementById('loadmore').innerHTML = 'Nema više usluga. Sve ste učitali :)';
                }
            }).catch(() => console.log('App is down'));
    }

    loadMore = () => {
        this.page++;
        this.searchUrl.searchParams.set('page', this.page);
        axios.get(this.searchUrl)
            .then(res => {
                this.setState({
                    services: [
                        ...this.state.services,
                        ...res.data
                    ]
                }, () => {
                    for(var index in this.state.services){
                        this.isUserPremuim(this.state.services[index]._id, this.state.services[index].owner)
                    }});
                if (res.data.length < 10) {
                    this.setState({
                        hasMore: false
                    });
                    document.getElementById('loadmore').innerHTML = 'Nema više usluga. Sve ste učitali :)';
                }
            }).catch(() => console.log('App is down'));
    }

    isUserPremuim(_id, username){
        axios.get('/api/users/isPremium/' + username).then(res => {
            if(res){
                this.state.services.filter(service => service._id === _id)[0].premium = res.data;
                this.setState({
                    services: this.state.services
                })
            }else{

            }
        })
    }

    render() {
        return (
            <div className="result-div">
                <Container>
                    <SearchBar />
                    <div style={{ "width": "80%", "margin": "0 auto" }}>
                        {
                            this.state.services.map(({ _id, title, description, owner, category, premium }) => (
                                <div key={_id} className={premium?"result-body-premium":"result-body"}>
                                    <div className="result-image"><a href={"/services/" + _id}><img src={icons[category]} alt=''></img></a></div>
                                    <div className="result-info">
                                        <a href={"/services/" + _id}>
                                            <h4 style={{ fontWeight: "bold", display: "inline", color: 'black' }}>{title + ' '}{premium?<Badge color='warning'>PREMIUM</Badge>:''}</h4>
                                        </a>
                                        <h5> Ponuđač: <Badge style={{ "cursor": "pointer" }} color='warning' onClick={() => { window.location.href = '/users/' + owner }}>{owner}</Badge></h5>
                                        <p>{
                                            description.length > 180 ?
                                                description.substr(0, 160) + ' ...' :
                                                description
                                        }</p>
                                    </div>
                                    <hr />
                                </div>
                            ))
                        }
                    </div>
                </Container>
                <div className='d-flex justify-content-center'>
                    <Spinner color='warning' hidden={this.state.loaded} />
                    <Alert style={{ width: '90%' }} color='dark'
                        hidden={this.state.loaded === false || this.state.services.length !== 0}>
                        <h5><b>Nije pronađeno :(</b></h5>
                        <hr />
                        <p>Ne postoji niti jedna usluga koja odgovara vašoj pretrazi</p>
                    </Alert>
                </div>
                <Button block id='loadmore' disabled={!this.state.hasMore} color='link' onClick={this.loadMore}
                    hidden={this.state.loaded === true && this.state.services.length === 0}>Učitaj još...</Button>
            </div >
        );
    }
}