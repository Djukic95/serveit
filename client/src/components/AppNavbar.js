import React, {
    Component,
    Fragment
} from 'react';
import {
    Collapse,
    Container,
    Nav,
    Navbar,
    NavItem,
    NavLink,
    NavbarBrand,
    NavbarToggler
} from 'reactstrap';
import LoginModal from './LoginModal';
import SignupModal from './SignupModal';

export default class AppNavbar extends Component {

    state = {
        isOpen: false,
        username: sessionStorage.getItem('username')
    }

    logout = () => {
        sessionStorage.clear();
        window.location.href = '/';
    }

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
            <div className='sticky-top mb-3'>
                <Navbar expand='sm' light>
                    <Container>
                        <NavbarBrand href='/'>
                            <img src='/sit-logo.png' alt='SERVE IT' />
                        </NavbarBrand>
                        <NavbarToggler onClick={this.toggle} />
                        <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav className='ml-auto' navbar>
                                <NavItem>
                                    <NavLink href='/'>POČETNA STRANA</NavLink>
                                </NavItem>
                                {this.state.username ?
                                    <Fragment>
                                        <NavItem>
                                            <NavLink href={`/users/${this.state.username}`}>
                                                {this.state.username.toUpperCase()}
                                            </NavLink>
                                        </NavItem>
                                        <NavItem className='hand' onClick={this.logout}>
                                            <NavLink>ODJAVI SE</NavLink>
                                        </NavItem>
                                    </Fragment> :
                                    <Fragment>
                                        <LoginModal />
                                        <SignupModal />
                                    </Fragment>
                                }
                            </Nav>
                        </Collapse>
                    </Container>
                </Navbar>
            </div>
        );
    }
}