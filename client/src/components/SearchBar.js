import React, {
    Component
} from 'react';
import {
    Button,
    Col,
    Container,
    Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Input,
    InputGroup,
    InputGroupButtonDropdown,
    Row
} from 'reactstrap';
import cities from '../static_data/cities';
import categories from '../static_data/categories';

export default class SearchBar extends Component {

    state = {
        categoriesOpen: false,
        citiesOpen: false,
        selectedCity: 'GRAD',
        selectedCategory: 'KATEGORIJA'
    }

    searchTerm = '';

    onKeyPress = e => {
        if (e.key === 'Enter') {
            this.onClick();
        }
    }

    categoriesToggle = () => {
        this.setState({
            categoriesOpen: !this.state.categoriesOpen
        });
    }

    citiesToggle = () => {
        this.setState({
            citiesOpen: !this.state.citiesOpen
        });
    }

    onChange = e => {
        this.searchTerm = e.target.value;
    }

    onClick = () => {
        this.searchTerm = this.searchTerm.trim()
        if (this.searchTerm ||
            this.state.selectedCity !== 'GRAD' ||
            this.state.selectedCategory !== 'KATEGORIJA') {
            let url = new URL(`${window.location.origin}/search`);
            if (this.searchTerm) {
                url.searchParams.set('q', this.searchTerm);
            }
            if (this.state.selectedCategory !== 'KATEGORIJA') {
                url.searchParams.set('category', this.state.selectedCategory);
            }
            if (this.state.selectedCity !== 'GRAD') {
                url.searchParams.set('city', this.state.selectedCity);
            }
            window.location.href = url;
        }
    }

    render() {
        return (
            <Container fluid className='mb-3'>
                <Row className="search-bar">
                    <Col xs={2.5}>
                        <Button color='warning' outline onClick={() => {
                            window.location.href = '/add';
                        }} className='ml-5 mr-3'>DODAJ USLUGU</Button>
                    </Col>
                    <Col xs={2}>
                        <Dropdown isOpen={this.state.citiesOpen} toggle={this.citiesToggle}>
                            <DropdownToggle block outline caret color='warning'>{this.state.selectedCity}</DropdownToggle>
                            <DropdownMenu style={{ maxHeight: '250px', overflow: 'auto' }}>
                                <DropdownItem key='grad' onClick={() => {
                                                this.setState({
                                                    selectedCity: 'GRAD'
                                                })
                                            }}>GRAD</DropdownItem>
                                {
                                    cities.map((city) => (
                                        <DropdownItem key={city} onClick={() => {
                                            this.setState({
                                                selectedCity: city
                                            });
                                        }}>{city}</DropdownItem>
                                    ))
                                }
                            </DropdownMenu>
                        </Dropdown>
                    </Col>
                    <Col xs>
                        <InputGroup>
                            <Input placeholder="Pronađimo najbolju uslugu za Vas..."
                                onChange={this.onChange} onKeyPress={this.onKeyPress} />
                            <InputGroupButtonDropdown addonType="append" isOpen={this.state.categoriesOpen} toggle={this.categoriesToggle}>
                                <DropdownToggle caret color='warning'>{this.state.selectedCategory}</DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem key='jova' onClick={() => {
                                                this.setState({
                                                    selectedCategory: 'KATEGORIJA'
                                                })
                                            }}>KATEGORIJA</DropdownItem>
                                    {
                                        categories.map((cat) => (
                                            <DropdownItem key={cat} onClick={() => {
                                                this.setState({
                                                    selectedCategory: cat
                                                })
                                            }}>{cat}</DropdownItem>
                                        ))
                                    }
                                </DropdownMenu>
                            </InputGroupButtonDropdown>
                        </InputGroup>
                    </Col>
                    <Col xs={2}>
                        <Button style={{ "width": "80%" }} color='warning' onClick={this.onClick}>PRETRAŽI</Button>
                    </Col>
                </Row>
            </Container>
        );
    }
}