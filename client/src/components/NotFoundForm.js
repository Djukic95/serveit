import React, {
    Component
} from 'react';
import {
    Container,
    Button
} from 'reactstrap';

export default class NotFoundForm extends Component {

    render() {
        return (
            <Container style={{ width: '33%' }}>
                <img src='https://image.flaticon.com/icons/svg/868/868792.svg' alt='' />
                <div className='d-flex justify-content-center'>
                    Stranica nije pronađena
                </div>
                <Button outline block color='warning' size='lg' tag='a' href='/'>
                    VRATI SE NA POČETNU
                </Button>
            </Container>
        );
    }
}