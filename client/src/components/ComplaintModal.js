import React, {
    Component
} from 'react';
import {
    Alert,
    Button,
    Container,
    Form,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalHeader,
    ModalBody,
    Spinner
} from 'reactstrap';
import axios from 'axios';

export default class ComplaintModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            clicked: false,
            success: false,
            reason: ''
        };
        this.service = props.service;
    }

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    onChange = (e) => {
        this.setState({
            reason: e.target.value
        });
    }

    onSubmit = (e) => {
        e.preventDefault();

        this.setState({
            clicked: true,
        });

        axios.post('/api/complaints', {
            service: this.service,
            reason: this.state.reason
        }, { headers: { authorization: `Bearer ${sessionStorage.getItem('Bearer')}` } })
            .then(res => {
                if (res.status === 200) {
                    this.setState({
                        success: true
                    });
                    setTimeout(() => {
                        this.reason = '';
                        this.toggle();
                    }, 5000);
                }
            });
    }

    render() {
        return (
            <div>
                <Button outline size='sm' color='danger' className='float-right'
                    onClick={this.toggle}>PRIJAVI USLUGU</Button>
                <Modal isOpen={this.state.isOpen} toggle={this.toggle} autoFocus={false}>
                    <ModalHeader toggle={this.toggle}>PRIJAVI USLUGU</ModalHeader>
                    <ModalBody>
                        <Container className='container-form'>
                            <Form onSubmit={this.onSubmit}>
                                <FormGroup>
                                    <Label size='sm' for='reason'>Upišite razlog prijave</Label>
                                    <Input bsSize='sm' type='textarea' id='reason' value={this.reason} disabled={this.state.clicked} onChange={this.onChange} autoFocus />
                                </FormGroup>
                                <FormGroup>
                                    <Alert color='success' hidden={!this.state.success}>
                                        Hvala Vam što nam pomažete ukloniti loš sadržaj
                                        <br />
                                        Vaša prijava će biti obrađena u najkraćem roku
                                    </Alert>
                                    {
                                        this.state.clicked ?
                                            <div className='d-flex justify-content-center'>
                                                <Spinner hidden={this.state.success} color='warning' />
                                            </div> :
                                            <Button outline color='warning' size='sm' block>POŠALJI PRIJAVU</Button>
                                    }
                                </FormGroup>
                            </Form>
                        </Container>
                    </ModalBody>
                </Modal>
            </div>
        );
    }
}