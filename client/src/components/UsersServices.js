import React, {
    Component
} from 'react';
import {
    Card,
    CardBody,
    CardImg,
    CardTitle,
    Col,
    Container,
    Row
} from 'reactstrap';

import {
    ContextMenu,
    ContextMenuTrigger,
    MenuItem
} from 'react-contextmenu';
import axios from 'axios';
import icons from '../static_data/icons';

export default class UsersServices extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: props.username,
            services: [],
            removed: false
        };

        this.removeService = this.removeService.bind(this);
        this.editService = this.editService.bind(this);
    }

    componentWillMount() {
        axios.get(`/api/services/?owner=${this.state.username}`)
            .then(res => {
                this.setState({
                    services: res.data
                });
            }).catch(() => console.log('App is down'));
    }

    removeService(e, data) {
        if (window.confirm("Da li ste sigurni da želite ukloniti uslugu?")) {
            axios.delete('/api/services/' + data.id, {
                headers: {
                    authorization: `Bearer ${sessionStorage.getItem('Bearer')}`
                }
            }).then(res => {
                if (res.status === 200) {
                    this.setState({
                        removed: true,
                        services: this.state.services.filter(service => service.id !== data.id)
                    }, () => { document.getElementById('card-' + data.id).hidden = true; })
                }
            }).catch((_err) => console.log(_err));
        }
    }

    editService(e, data) {
        sessionStorage.setItem('serviceEditId', data.id);
        document.location.href = '/edit/service/' + data.id;
    }

    render() {
        return (
            <Container className='container-info mt-5'>
                <Row>
                    {
                        this.state.services.map(({ _id, title, category }) => (

                            <Col id={'card-' + _id} xs={3} key={_id}>
                                <div className='hand' onClick={() => {
                                    window.location.href = `/services/${_id}`;
                                }}>
                                    <ContextMenuTrigger id={_id}>
                                        <Card className='mb-3' style={{ "zIndex": "-1" }}>
                                            <CardImg style={{ "padding": "5px" }} top src={icons[category]} alt='SERVICE' />
                                            <CardBody>
                                                <CardTitle className='href'>{title}</CardTitle>
                                            </CardBody>
                                        </Card>
                                    </ContextMenuTrigger>
                                </div>
                                {
                                    this.state.username === sessionStorage.getItem('username') ?
                                        <ContextMenu className="edit-context-menu" id={_id}>
                                            <MenuItem data={{ id: _id }} onClick={this.editService}><span className="edit-menu-item">Izmijeni opis</span></MenuItem>
                                            <hr />
                                            <MenuItem data={{ id: _id }} onClick={this.removeService}><span className="edit-menu-item">Ukloni uslugu</span></MenuItem>
                                        </ContextMenu> : null
                                }
                            </Col>
                        ))
                    }
                </Row>
            </Container>
        );
    }
}