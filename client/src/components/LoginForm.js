import React, {
    Component
} from 'react';
import {
    Alert,
    Button,
    Container,
    Form,
    FormGroup,
    Input,
    Label,
    Spinner
} from 'reactstrap';
import axios from 'axios';

export default class LoginForm extends Component {
    state = {
        alert: false,
        clicked: false
    }

    credentials = {}

    onChange = (e) => {
        this.credentials[e.target.id] = e.target.value
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.setState({
            alert: false,
            clicked: true
        });

        axios.post('/api/login', this.credentials)
            .then(res => {
                if (typeof res.data === 'string') {
                    sessionStorage.setItem('Bearer', res.data);
                    sessionStorage.setItem('username', this.credentials.username);
                    window.location.reload();
                } else {
                    this.setState({
                        alert: true,
                        clicked: false
                    });
                }
            }).catch(_err => {
                this.setState({
                    alert: true,
                    clicked: false
                });
            });
    }

    render() {
        return (
            <Container className='container-form'>
                <Form onSubmit={this.onSubmit}>
                    <FormGroup>
                        <Label size='sm' for='username'>Korisničko ime</Label>
                        <Input bsSize='sm' id='username' disabled={this.state.clicked} onChange={this.onChange} autoFocus />
                    </FormGroup>
                    <FormGroup>
                        <Label size='sm' for='password'>Lozinka</Label>
                        <Input bsSize='sm' type='password' disabled={this.state.clicked} id='password' onChange={this.onChange} />
                    </FormGroup>
                    <FormGroup>
                        <Alert color='danger' hidden={!this.state.alert}>
                            Pogrešno ste unijeli kredencijale
                            </Alert>
                    </FormGroup>
                    <FormGroup>
                        {this.state.clicked ?
                            <div className="d-flex justify-content-center">
                                <Spinner color='warning' />
                            </div> :
                            <Button outline block color='warning' size='sm'>PRIJAVI SE</Button>
                        }
                    </FormGroup>
                </Form>
            </Container>
        );
    }
}