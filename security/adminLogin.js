const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const Admin = require('../models/Admin');

module.exports = (credentials, res) => {
    Admin.findOne({
        username: credentials.username
    }, 'password').then(admin => {
        if (admin) {
            bcrypt.compare(credentials.password, admin.password)
                .then(auth => {
                    if (auth) {
                        jwt.sign({ username: credentials.username },
                            'sit_token_password',
                            (_err, token) => res.json(token));
                    } else { res.sendStatus(401) }
                }).catch(() => res.sendStatus(503));
        } else { res.sendStatus(401) }
    }).catch(() => res.sendStatus(503));
}