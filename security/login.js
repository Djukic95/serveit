const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../models/User');

module.exports = (credentials, res) => {
    User.findOne({
        username: credentials.username,
        active: true
    }, 'password').then(user => {
        if (user) {
            bcrypt.compare(credentials.password, user.password)
                .then(auth => {
                    if (auth) {
                        jwt.sign({ username: credentials.username },
                            'sit_token_password',/* { expiresIn: '1h' },*/
                            (_err, token) => res.json(token));
                    } else { res.json(401) }
                }).catch(() => res.json(503));
        } else { res.json(401) }
    }).catch(() => res.json(503));
}