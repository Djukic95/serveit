const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    bearerHeader = req.headers.authorization;
    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(' ')[1];
        jwt.verify(bearerToken, 'sit_token_password', (err, authData) => {
            if (err) {
                res.sendStatus(401);
            } else {
                req.username = authData.username;
                next();
            }
        });
    } else {
        res.sendStatus(401);
    }
}