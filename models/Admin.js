const mongoose = require('mongoose');

const AdminSchema = new mongoose.Schema({
    username:{
        type: String,
        required: true,
        unique:true,
        match: /[a-zA-Z]\w{1,31}/
    },
    password:{
        type: String,
        required:true
    }
}, {versionKey: false});

module.exports = mongoose.model('Admin', AdminSchema);