const mongoose = require('mongoose');

const ComplaintSchema = new mongoose.Schema({
    user: {
        type: String,
        required: true
    },
    service: {
        type: String,
        required: true
    },
    reason: {
        type: String,
        maxlength: 512
    },
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Complaint', ComplaintSchema);