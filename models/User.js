const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true,
        match: /[a-zA-Z]\w{1,31}/
    },
    name: {
        type: String,
        required: true,
    },
    surname: {
        type: String,
        required: true
    },
    contact: {
        email: {
            type: String,
            default: ''
        },
        phone: {
            type: String,
            default: ''
        },
        website: {
            type: String,
            default: ''
        }
    },
    street: String,
    city: {
        type: String,
        required: true
    },
    isEnterprise: {
        type: Boolean,
        default: false
    },
    created: {
        type: Date,
        default: Date.now
    },
    active: {
        type: Boolean,
        default: true
    },
    businessHours: {
        weekday: {
            type: String,
            default: ''
        },
        saturday: {
            type: String,
            default: ''
        },
        sunday: {
            type: String,
            default: ''
        }
    },
    password: {
        type: String,
        required: true
    }
}, { versionKey: false });

module.exports = mongoose.model('User', UserSchema);