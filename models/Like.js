const mongoose = require('mongoose');

const LikeSchema = new mongoose.Schema({
    username: String,
    service: String
});

module.exports = mongoose.model('Like', LikeSchema);