const mongoose = require('mongoose');

const ServiceSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        maxlength: 256
    },
    date: {
        type: Date,
        default: Date.now
    },
    category: {
        type: String,
        required: true,
        maxlength: 64
    },
    description: {
        type: String,
        default: ''
    },
    active: {
        type: Boolean,
        default: true
    },
    rating: {
        positive: {
            type: Number,
            default: 0
        },
        negative: {
            type: Number,
            default: 0
        }
    },
    comments: [{
        body: String,
        author: {
            type: String,
            required: true
        },
        date: {
            type: Date,
            default: Date.now
        }
    }],
    owner: {
        required: true,
        type: String,
    }
}, { versionKey: false });

module.exports = mongoose.model('Service', ServiceSchema);