const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const sslRedirect = require('heroku-ssl-redirect');

const services = require('./routes/services');
const users = require('./routes/users');
const likes = require('./routes/likes');
const login = require('./routes/login');
const admin = require('./routes/admin');
const complaints = require('./routes/complaint');
const mongoURI = require('./config/keys').mongoURI;

const port = process.env.PORT || 5000;
const app = express();

mongoose.connect(mongoURI, {
    useNewUrlParser: true,
    useCreateIndex: true
})
    .then(() => console.log('MongoDB connected'))
    .catch(() => console.log('MongoDB unavailable'));

app.use(bodyParser.json());
app.use(sslRedirect());
app.use('/api/services', services);
app.use('/api/users', users);
app.use('/api/likes', likes);
app.use('/api/login', login);
app.use('/api/admin', admin);
app.use('/api/complaints', complaints);
app.use(express.static('./client/build'));
app.get('*', (_req, res) => res.sendFile(`${__dirname}/client/build/index.html`));

app.listen(port, () => console.log(`Server started on port ${port}`));