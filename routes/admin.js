const express = require('express');
const bcrypt = require('bcryptjs');
const adminLogin = require('../security/adminLogin');
const router = express.Router();
const Admin = require('../models/Admin');

router.post('/', (req, res) => {
    adminLogin(req.body, res);
});

router.post('/new', (req, res) => {
    bcrypt.hash(req.body.password, 10)
        .then(hash => {
            req.body.password = hash;
            new Admin(req.body).save()
                .then(() => res.sendStatus(200))
                .catch(() => res.sendStatus(409));
        })
        .catch(() => res.sendStatus(503));
});

router.get('/', (req, res) =>{
    Admin.find().then((admins) =>{
        res.json(admins);
    }).catch(() => res.sendStatus(404));
});

module.exports = router;