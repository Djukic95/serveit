const express = require('express');
const login = require('../security/login');
const router = express.Router();

router.post('/', (req, res) => {
    login(req.body, res);
});

module.exports = router;