const express = require('express');
const Like = require('../models/Like');
const Service = require('../models/Service');
const auth = require('../security/auth');
const router = express.Router();

router.post('/like', auth, (req, res) => {
    Like.countDocuments({
        service: req.body.service,
        username: req.username
    })
        .then(count => {
            if (count) {
                res.sendStatus(403);
            } else {
                new Like({
                    service: req.body.service,
                    username: req.username
                }).save();
                Service.updateOne({ _id: req.body.service }, {
                    $inc: { 'rating.positive': 1 }
                }, (_err, _raw) => {
                    res.sendStatus(200);
                });
            }
        }).catch(() => res.sendStatus(503));
});

router.post('/dislike', auth, (req, res) => {
    Like.countDocuments({
        service: req.body.service,
        username: req.username
    })
        .then(count => {
            if (count) {
                res.sendStatus(403);
            } else {
                new Like({
                    service: req.body.service,
                    username: req.username
                }).save();
                Service.updateOne({ _id: req.body.service }, {
                    $inc: { 'rating.negative': 1 }
                }, (_err, _raw) => {
                    res.sendStatus(200);
                });
            }
        }).catch(() => res.sendStatus(503));
});

module.exports = router;