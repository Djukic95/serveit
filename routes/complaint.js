const express = require('express');
const Complaint = require('../models/Complaint');
const Service = require('../models/Service');
const auth = require('../security/auth');
const router = express.Router();

router.post('/', auth, (req, res) => {
    new Complaint({
        user: req.username,
        service: req.body.service,
        reason: req.body.reason
    }).save()
        .then(() => res.sendStatus(200))
        .catch(() => res.sendStatus(503));
});

router.get('/service/:id', (req, res) => {
    Complaint.find({ service: req.params.id })
        .then(complaints => res.json(complaints))
        .catch(() => res.sendStatus(404));
});

router.get('/user/:username', (req, res) => {
    Service.find({ owner: req.params.username })
        .then(services => {
            Complaint.find({ service: { $in: services } })
                .then(complaints => res.json(complaints))
                .catch(() => res.sendStatus(404));
        }).catch(() => res.sendStatus(404));
});

router.get('/all', (req, res) => {
    Complaint.find()
        .then(complaints => res.json(complaints))
        .catch(() => res.sendStatus(404));
});

module.exports = router;