const express = require('express');
const Service = require('../models/Service');
const User = require('../models/User');
const auth = require('../security/auth');
const router = express.Router();
const stringSimilarity = require('string-similarity');

router.get('/', async (req, res) => {
    query = { active: true };
    const { q, owner, category, city, page } = req.query;
    // if (q) { query.title = RegExp(q, 'i'); }
    if (owner) { query.owner = owner; }
    if (category) { query.category = RegExp(category, 'i'); }
    if (city) {
        const owners = await User.find({ city }, 'username');
        query.owner = {
            $in: owners.map(({ username }) => username)
        }
    }

    Service.find(query)
        .sort({ date: -1 })
        .then((services) => {
            if (q) services = services.map((service) => ({
                service: service,
                similarity: stringSimilarity.compareTwoStrings(service.title, q)
            }))
                .filter(({ similarity }) => (similarity > 0.32))
                .sort((a, b) => (b.similarity - a.similarity))
                .map(({ service }) => service);
            if (page) {
                services = services.slice((page - 1) * 10, page * 10);
            }
            res.json(services);
        })
        .catch(() => res.sendStatus(503));
});

router.get('/all', (_req, res) => {
    Service.find({})
        .then(services => res.json(services))
        .catch(() => res.sendStatus(503));
});

router.get('/last/:num', (req, res) => {
    Service.find({ active: true })
        .sort({ date: -1 })
        .limit(Number(req.params.num))
        .then((services) => {
            res.json(services)
        }).catch(() => res.sendStatus(503));
});

router.get('/count', (_req, res) => {
    Service.countDocuments({ active: true })
        .then((count) => res.json(count))
        .catch(() => res.json(0));
});

router.get('/:id', (req, res) => {
    Service.findById(req.params.id)
        .then((service) => {
            res.json(service);
        }).catch(() => res.sendStatus(404));
});

router.post('/:id/comment', auth, (req, res) => {
    const comm = {
        author: req.username,
        body: req.body.comment
    }
    Service.updateOne({ _id: req.params.id }, {
        $push: {
            comments: {
                $each: [comm],
                $position: 0
            }
        }
    }, (err, _raw) => {
        if (err) {
            res.sendStatus(503);
        } else {
            res.sendStatus(200);
        }
    });
});

router.post('/new', auth, (req, res) => {
    const serv = req.body;
    if (serv.comments || serv.rating || serv.active || serv.date) {
        res.sendStatus(403);
    } else {
        serv.owner = req.username;
        new Service(req.body).save()
            .then(() => res.sendStatus(200))
            .catch(() => res.sendStatus(409));
    }
});

//toggles the active state of the service
router.put('/toggleActive', auth, (req, res) => {
    const serv = req.body;
    Service.findOne({ _id: serv._id }).then(service => {
        if (service) {
            var newActiveState = !service.active;
            Service.updateOne({ _id: serv._id }, { active: newActiveState },
                (_err, raw) => res.sendStatus(raw.nModified ? 200 : 401));
        }
    }).catch(() => res.sendStatus(403));
})

router.put('/:id', auth, (req, res) => {
    const serv = req.body;
    if (serv.rating || serv.active || serv.owner || serv.comments) {
        res.sendStatus(403);
    } else {
        serv.date = Date.now();
        Service.updateOne({ _id: req.params.id, owner: req.username }, serv,
            (_err, raw) => res.sendStatus(raw.nModified ? 200 : 403));
    }
});


router.delete('/:id', auth, (req, res) => {
    Service.updateOne({ _id: req.params.id, owner: req.username }, { active: false },
        (_err, raw) => res.sendStatus(raw.nModified ? 200 : 403));
});

module.exports = router;