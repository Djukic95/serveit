const express = require('express');
const bcrypt = require('bcryptjs');
const User = require('../models/User');
const Service = require('../models/Service');
const auth = require('../security/auth');
const router = express.Router();

router.get('/', (_req, res) => {
    User.find({ active: true }, '-password')
        .then(users => res.json(users))
        .catch(() => res.sendStatus(503));
});

router.get('/all', (_req, res) => {
    User.find({}, '-password')
        .then(users => res.json(users))
        .catch(() => res.sendStatus(503));
});

router.get('/count', (_req, res) => {
    User.countDocuments({ active: true })
        .then((count) => res.json(count))
        .catch(() => res.json(0));
});

router.get('/isPremium/:username', (req, res) => {
    User.findOne({ username: req.params.username }, '-password')
        .then((user) => {
            if (user) {
                res.json(user.isEnterprise);
            } else {
                res.sendStatus(404);
            }
        }).catch(() => res.sendStatus(503));
});

router.get('/:username', (req, res) => {
    User.findOne({ username: req.params.username }, '-password')
        .then((user) => {
            if (user) {
                res.json(user);
            } else {
                res.sendStatus(404);
            }
        }).catch(() => res.sendStatus(503));
});

router.post('/new', (req, res) => {
    bcrypt.hash(req.body.password, 10)
        .then(hash => {
            req.body.password = hash;
            new User(req.body).save()
                .then(() => res.sendStatus(200))
                .catch(() => res.sendStatus(409));
        })
        .catch(() => res.sendStatus(503));
});

//used for admin user filtering
router.post('/filter', (req, res) => {
    User.find(req.body).then(users => {
        res.json(users);
    }).catch(() => res.sendStatus(503));
});


//updates the user who sent the request
router.put('/me', auth, (req, res) => {
    const acc = req.body;
    if (acc.isEnterprise || acc.active || acc.username || acc.password) {
        res.sendStatus(403);
    } else {
        User.updateOne({ username: req.username }, acc,
            (_err, raw) => res.sendStatus(raw.nModified ? 200 : 304));
    }
});

// updates users password
router.post('/passwd', auth, (req, res) => {
    const { oldP, newP } = req.body;
    if (newP === '') {
        res.sendStatus(409);
    }
    User.findOne({ username: req.username }, 'password')
        .then(user => {
            if (bcrypt.compareSync(oldP, user.password)) {
                const password = bcrypt.hashSync(newP, 10);
                User.updateOne({ username: req.username }, { password },
                    (_err, _raw) => res.sendStatus(200));
            } else {
                res.sendStatus(409);
            }
        }).catch(() => res.sendStatus(503));
});

//toggles the active state of the user
router.put('/toggleActive', auth, (req, res) => {
    const acc = req.body;
    User.findOne({ username: acc.username }).then(user => {
        if (user) {
            var newActiveState = !user.active;
            User.updateOne({ username: user.username }, { active: newActiveState },
                (_err, raw) => res.sendStatus(raw.nModified ? 200 : 401));
        }
    }).catch(() => res.sendStatus(403));
    Service.updateMany({ owner: acc.username }, { active: false }).exec();
});

router.put('/togglePremium', auth, (req, res) => {
    const acc = req.body;
    User.findOne({ username: acc.username }).then(user => {
        if (user) {
            var newPremiumState = !user.isEnterprise;
            User.updateOne({ username: user.username }, { isEnterprise: newPremiumState },
                (_err, raw) => res.sendStatus(raw.nModified ? 200 : 401));
        }
    }).catch(() => res.sendStatus(403));
});

//deactivates the user who sent the request
router.delete('/me', auth, (req, res) => {
    User.updateOne({ username: req.username }, { active: false },
        (_err, raw) => res.sendStatus(raw.nModified ? 200 : 403));
    Service.updateMany({ owner: req.username }, { active: false }).exec();
});

module.exports = router;